#!/usr/bin/env python3

def fuzzy_logic_module (answers_array, f):

    num_usu = len(answers_array)
    num_sub = 0
    cant_items = []
    for i in range(len(answers_array)):
        for j in range(len(answers_array[i])):
            num_sub = num_sub + 1 
        cant_items.append(len(answers_array[i]))


    #Fuzzyfication

    fuzzyficated_answers = []
    for i in range(len(answers_array)):
        row1 = []
        for j in range(len(answers_array[i])):
            row2 = []
            for k in range(len(answers_array[i][j])):
                if answers_array[i][j][k] == 1:
                    row2.append(0)
                elif answers_array[i][j][k] == 2:
                    row2.append(0.25)
                elif answers_array[i][j][k] == 3:
                    row2.append(0.50)
                elif answers_array[i][j][k] == 4:
                    row2.append(0.75)
                elif answers_array[i][j][k] == 5:
                    row2.append(1)    
            row1.append(row2)
        fuzzyficated_answers.append(row1)


    #Inference

    inference_list = []
    min_list = []
    max_list = []
    prom_list = []
    cant_items_len = 0

    for i in range(len(fuzzyficated_answers)):
        row_min = []
        row_max = []
        row_prom = []
        for j in range(len(fuzzyficated_answers[i])):
            min = 2
            max = -1
            sum = 0
            for k in range(len(fuzzyficated_answers[i][j])):
                if fuzzyficated_answers[i][j][k] < min:
                    min = fuzzyficated_answers[i][j][k]
                if fuzzyficated_answers[i][j][k] > max:
                    max = fuzzyficated_answers[i][j][k]
                sum = sum + fuzzyficated_answers[i][j][k]
            prom = (sum / num_usu)
            row_min.append(min)
            row_max.append(max)
            row_prom.append(prom)
        min_list.append(row_min)
        max_list.append(row_max)
        prom_list.append(row_prom)

    #Convirtiendo promedios

    new_prom_list = []
    for i in range(len(prom_list)):
        new_row_prom = []
        for j in range(len(prom_list[i])):
            if prom_list[i][j] <= 0.25:
                if 0.25 - prom_list[i][j] > 0.125:
                    new_row_prom.append(0)
                else:
                    new_row_prom.append(0.25)
            elif prom_list[i][j] > 0.25 and prom_list[i][j] <= 0.50:
                if 0.50 - prom_list[i][j] > 0.125:
                    new_row_prom.append(0.25)
                else:
                    new_row_prom.append(0.50)
            elif prom_list[i][j] > 0.50 and prom_list[i][j] <= 0.75:
                if 0.75 - prom_list[i][j] > 0.125:
                    new_row_prom.append(0.50)
                else:
                    new_row_prom.append(0.75)
            elif prom_list[i][j] > 0.75 and prom_list[i][j] <= 1:
                if 1 - prom_list[i][j] > 0.125:
                    new_row_prom.append(0.75)
                else:
                    new_row_prom.append(1)
        new_prom_list.append(new_row_prom)


    #Defuzzification

    subfactor_list = []
    for i in range(len(min_list)):
        defuzzification_row = []
        for j in range(len(min_list[i])):
            subfactor = 0.25 * min_list[i][j] + 0.5 * new_prom_list[i][j] + 0.25 * max_list[i][j]
            subfactor = round(subfactor,2)
            defuzzification_row.append(subfactor)
        subfactor_list.append(defuzzification_row)

    f.write("\nSubfactores = " + str(subfactor_list))

    factor_list = []
    for i in range(len(subfactor_list)):
        sum = 0
        for j in range(len(subfactor_list[i])):
            sum = sum + subfactor_list[i][j]
        prom = sum / len(subfactor_list[i])
        prom = round(prom,2)
        factor_list.append(prom)

    f.write("\nFactores = " + str(factor_list))

    sum = 0
    for i in range (len(factor_list)):
        sum = sum + factor_list[i]

    usability = round(sum / len(factor_list),2)

    return usability



if __name__ == "__main__":

    print("\nAlumno: Manuel Delgado Alba")
    print("Código: 20140631")
    print("Fecha: 24/09/2020")

    print("\nProyecto de Tesis 2 - Fuzzybility")

    print("\nLos resultados los puede visualizar en el archivo de texto creado en la misma ruta donde se encuentra este archivo")
    print("El nombre del archivo es Resultados_Modulo_Fuzzificacion_Desfuzzificacion.txt\n")

    f= open("Resultados_Modulo_Fuzzificacion_Desfuzzificacion.txt","w+")


    f.write("\n********************************************************")
    f.write("\nResultados del Módulo de Fuzzificación y Desfuzzificación")
    f.write("\n********************************************************\n\n")

    f.write("\n********************************************************")
    f.write("\nConjunto de Prueba 1")
    f.write("\n********************************************************")
    answers_array = [[[4, 3, 2],[3, 2, 4],[3, 3, 2],[3, 4, 4]],[[4, 3, 3],[4, 3, 4],[4, 2, 3],[4, 4, 4],[1, 1, 1]],
                [[3, 3, 3],[3, 2, 2],[3, 2, 2],[3, 3, 3],[3, 3, 3]]]
    usability = fuzzy_logic_module(answers_array, f)
    f.write("\nLa usabilidad del sistema utilizando el Conjunto de Prueba 1 es: " + str(usability))
    f.write("\n")

    f.write("\n********************************************************")
    f.write("\nConjunto de Prueba 2")
    f.write("\n********************************************************")
    answers_array = [[[4, 5, 2],[2, 4, 4],[3, 4, 2],[4, 3, 4]],[[3, 4, 3],[3, 4, 4],[2, 4, 3],[4, 4, 4],[5, 4, 4]],
                [[4, 3, 3],[3, 4, 4],[3, 3, 4],[4, 4, 3],[3, 3, 3]]]
    usability = fuzzy_logic_module(answers_array, f)
    f.write("\nLa usabilidad del sistema utilizando el Conjunto de Prueba 2 es: "+ str(usability))
    f.write("\n")

    f.write("\n********************************************************")
    f.write("\nConjunto de Prueba 3")
    f.write("\n********************************************************")
    answers_array = [[[5, 5, 5],[4, 4, 4],[3, 3, 3],[2, 2, 2]],[[1, 1, 1],[5, 3, 2],[4, 2, 1],[3, 5, 4],[2, 4, 3]],
                [[1, 3, 5],[5, 2, 4],[3, 3, 3],[2, 5, 2],[5, 3, 3]]]
    usability = fuzzy_logic_module(answers_array, f)
    f.write("\nLa usabilidad del sistema utilizando el Conjunto de Prueba 3 es: "+ str(usability))
    f.write("\n")

    f.write("\n********************************************************")
    f.write("\nConjunto de Prueba 4")
    f.write("\n********************************************************")
    answers_array = [[[3, 3, 5],[5, 2, 4],[4, 1, 3],[3, 5, 2]],[[2, 3, 1],[1, 5, 3],[3, 3, 2],[2, 2, 5],[5, 1, 4]],
                [[4, 2, 3],[3, 2, 4],[2, 5, 3],[1, 4, 5],[5, 3, 5]]]
    usability = fuzzy_logic_module(answers_array, f)
    f.write("\nLa usabilidad del sistema utilizando el Conjunto de Prueba 4 es: "+ str(usability))
    f.write("\n")

    f.write("\n********************************************************")
    f.write("\nConjunto de Prueba 5")
    f.write("\n********************************************************")
    answers_array = [[[2, 4, 5],[1, 3, 3],[3, 2, 2],[2, 1, 4]],[[5, 2, 3],[3, 1, 3],[2, 3, 4],[2, 2, 4],[1, 5, 4]],
                [[2, 3, 3],[2, 2, 4],[5, 3, 3],[4, 2, 3],[3, 3, 2]]]
    usability = fuzzy_logic_module(answers_array, f)
    f.write("\nLa usabilidad del sistema utilizando el Conjunto de Prueba 5 es: "+ str(usability))
    f.write("\n")

    f.write("\n********************************************************")
    f.write("\nConjunto de Prueba 6")
    f.write("\n********************************************************")
    answers_array = [[[1, 5, 1],[5, 1, 4],[2, 3, 3],[3, 2, 3]],[[5, 3, 2],[3, 2, 5],[2, 5, 4],[1, 4, 3],[2, 3, 3]],
                [[1, 3, 2],[3, 2, 5],[2, 3, 3],[5, 5, 5],[3, 3, 4]]]
    usability = fuzzy_logic_module(answers_array, f)
    f.write("\nLa usabilidad del sistema utilizando el Conjunto de Prueba 6 es: "+ str(usability))
    f.write("\n")

    f.write("\n********************************************************")
    f.write("\nConjunto de Prueba 7")
    f.write("\n********************************************************")
    answers_array = [[[2, 5, 3],[1, 3, 2],[2, 2, 5],[5, 1, 4]],[[4, 3, 2],[3, 5, 1],[2, 1, 4],[1, 3, 3],[5, 2, 3]],
                [[4, 3, 2],[5, 2, 5],[4, 5, 4],[3, 3, 2],[3, 2, 5]]]
    usability = fuzzy_logic_module(answers_array, f)
    f.write("\nLa usabilidad del sistema utilizando el Conjunto de Prueba 7 es: "+ str(usability))
    f.write("\n")

    f.write("\n********************************************************")
    f.write("\nConjunto de Prueba 8")
    f.write("\n********************************************************")
    answers_array = [[[2, 3, 3],[5, 5, 5],[5, 3, 4],[4, 2, 3]],[[3, 3, 3],[1, 5, 5],[5, 1, 4],[2, 3, 2],[3, 2, 5]],
                [[5, 3, 2],[1, 3, 3],[3, 2, 5],[2, 3, 3],[5, 5, 2]]]
    usability = fuzzy_logic_module(answers_array, f)
    f.write("\nLa usabilidad del sistema utilizando el Conjunto de Prueba 8 es: "+ str(usability))
    f.write("\n")

    f.write("\n********************************************************")
    f.write("\nConjunto de Prueba 9")
    f.write("\n********************************************************")
    answers_array = [[[1, 3, 1],[1, 5, 2],[4, 3, 2],[5, 2, 5]],[[4, 1, 4],[3, 3, 1],[3, 5, 2],[1, 3, 2],[3, 2, 1]],
                [[2, 5, 3],[3, 5, 2],[5, 5, 1],[1, 3, 3],[3, 2, 3]]]
    usability = fuzzy_logic_module(answers_array, f)
    f.write("\nLa usabilidad del sistema utilizando el Conjunto de Prueba 9 es: "+ str(usability))
    f.write("\n")

    f.write("\n********************************************************")
    f.write("\nConjunto de Prueba 10")
    f.write("\n********************************************************")
    answers_array = [[[2, 4, 4],[3, 3, 2],[2, 3, 3],[3, 4, 2]],[[3, 4, 3],[1, 3, 5],[2, 4, 1],[3, 3, 4],[2, 3, 2]],
                [[3, 2, 1],[5, 1, 3],[1, 4, 2],[3, 3, 3],[2, 3, 2]]]
    usability = fuzzy_logic_module(answers_array, f)
    f.write("\nLa usabilidad del sistema utilizando el Conjunto de Prueba 10 es: "+ str(usability))
    f.write("\n")